JavaScript
    本次學習，使用JS實作動態表格之新增、搜尋、編輯、刪除之功能(目前還有地方需要改善！))！並與之前實作的網頁結合！
    相關連結：https://gitlab.com/rock2337668006/web_page_practice.git

UML：Undefined Modeling Language
    個人認為此方法可有效幫助系統或軟體的開發！使用UML，能將大型軟體專案的架構、流程等，以簡潔的圖形化方式，呈現給專案開發相關參與者，以利軟體專 案開發的進行！目前有實作簡單的圖書館系統功能架構圖、圖書館使用者案例圖！
    實作連結：https://gitlab.com/rock2337668006/simple_library_dbs.git
    參考連結：https://zh.wikipedia.org/wiki/%E7%BB%9F%E4%B8%80%E5%BB%BA%E6%A8%A1%E8%AF%AD%E8%A8%80

Sprint Boot
    1. 我使用eclipse 2020_12版來建立Spring Boot專案，若eclipse版本過舊，可能會建立失敗！
    2. 目前已成功建立專案！

    參考連結：
    1. https://matthung0807.blogspot.com/2018/03/springbootweb.html
    2. https://zh.wikipedia.org/wiki/Spring_Framework




目前暫時寫到這邊，若大家發現有錯誤，歡迎指正！
    
